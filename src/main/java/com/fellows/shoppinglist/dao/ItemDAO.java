package com.fellows.shoppinglist.dao;

import com.fellows.shoppinglist.model.Item;
import com.fellows.shoppinglist.repository.ICrud;

public interface ItemDAO extends ICrud<String, Item> {

}
