package com.fellows.shoppinglist.dao;

import com.fellows.shoppinglist.model.ListItem;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;

public interface ListItemDAO {

    Set<LocalDateTime> getDates();

    void addPurchaseDate(LocalDateTime purchaseDate);

    void addListItemCollection(LocalDateTime purchaseDate, Collection<ListItem> listItems);

    void add(LocalDateTime purchaseDate, ListItem item);

    Collection<ListItem> getItems(LocalDateTime purchaseDate);

    boolean removeItem(LocalDateTime purchaseDate, ListItem item);

    boolean contains(LocalDateTime purchaseDate, ListItem item);
}
