package com.fellows.shoppinglist.dao.impl;

import com.fellows.shoppinglist.dao.ItemDAO;
import com.fellows.shoppinglist.model.Item;
import com.fellows.shoppinglist.repository.ItemRepository;
import com.fellows.shoppinglist.repository.impl.ItemHashSetRepositoryImpl;

import java.util.Collection;

public class ItemDAOImpl implements ItemDAO {

    private final ItemRepository repository;

    public ItemDAOImpl() {
        this(new ItemHashSetRepositoryImpl());
    }

    public ItemDAOImpl(ItemRepository repository) {
        this.repository = repository;
    }

    @Override
    public void add(Item entity) {
        repository.add(entity);
    }

    @Override
    public boolean remove(Item entity) {
        return repository.remove(entity);
    }

    @Override
    public boolean contains(Item entity) {
        return repository.contains(entity);
    }

    @Override
    public Item find(String name) {
        return repository.find(name);
    }

    @Override
    public Collection<Item> findAll() {
        return repository.findAll();
    }

    @Override
    public Collection<Item> findAll(String partial) {
        return repository.findAll(partial);
    }
}
