package com.fellows.shoppinglist.dao.impl;

import com.fellows.shoppinglist.dao.ShoppingListDAO;
import com.fellows.shoppinglist.model.ShoppingList;
import com.fellows.shoppinglist.repository.ShoppingListRepository;
import com.fellows.shoppinglist.repository.impl.ShoppingListHashSetRepositoryImpl;

import java.util.Collection;
import java.util.UUID;

public class ShoppingListDAOImpl implements ShoppingListDAO {

    private final ShoppingListRepository repository;

    public ShoppingListDAOImpl() {
        this(new ShoppingListHashSetRepositoryImpl());
    }

    public ShoppingListDAOImpl(ShoppingListRepository repository) {
        this.repository = repository;
    }

    @Override
    public void add(ShoppingList entity) {
        repository.add(entity);
    }

    @Override
    public boolean remove(ShoppingList entity) {
        return repository.remove(entity);
    }

    @Override
    public boolean contains(ShoppingList entity) {
        return repository.contains(entity);
    }

    @Override
    public ShoppingList find(UUID uuid) {
        return repository.find(uuid);
    }

    @Override
    public Collection<ShoppingList> findAll() {
        return repository.findAll();
    }

    @Override
    public Collection<ShoppingList> findAll(String partial) {
        return repository.findAll(partial);
    }
}
