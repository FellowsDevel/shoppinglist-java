package com.fellows.shoppinglist.dao;

import com.fellows.shoppinglist.model.ShoppingList;
import com.fellows.shoppinglist.repository.ICrud;

import java.util.UUID;

public interface ShoppingListDAO extends ICrud<UUID, ShoppingList> {
}
