package com.fellows.shoppinglist.repository;

import java.util.Collection;

public interface ICrud<ID, T> {
    void add(T entity);

    boolean remove(T entity);

    boolean contains(T entity);

    T find(ID id);

    Collection<T> findAll();

    Collection<T> findAll(String partial);
}
