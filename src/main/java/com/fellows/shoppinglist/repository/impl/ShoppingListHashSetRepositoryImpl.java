package com.fellows.shoppinglist.repository.impl;

import com.fellows.shoppinglist.model.ShoppingList;
import com.fellows.shoppinglist.repository.ShoppingListRepository;

import java.util.*;
import java.util.stream.Collectors;

public class ShoppingListHashSetRepositoryImpl implements ShoppingListRepository {

    private final Map<UUID, ShoppingList> items;

    public ShoppingListHashSetRepositoryImpl() {
        items = new HashMap<>();
    }

    @Override
    public void add(ShoppingList entity) {
        if (find(entity.getUid()) == null) {
            items.put(entity.getUid(), entity);
        }
    }

    @Override
    public boolean remove(ShoppingList entity) {
        return items.remove(entity.getUid()) != null;
    }

    @Override
    public boolean contains(ShoppingList entity) {
        return items.containsKey(entity.getUid());
    }

    @Override
    public ShoppingList find(UUID uuid) {
        return items.get(uuid);
    }

    @Override
    public Collection<ShoppingList> findAll() {
        return items.values().stream().sorted().toList();
    }

    @Override
    public Collection<ShoppingList> findAll(String partial) {
        return items.values()
                .stream()
                .filter(sl -> sl.getUid().toString().contains(partial))
                .collect(Collectors.toSet());
    }
}
