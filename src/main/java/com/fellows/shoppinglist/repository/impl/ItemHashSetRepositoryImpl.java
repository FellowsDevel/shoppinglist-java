package com.fellows.shoppinglist.repository.impl;

import com.fellows.shoppinglist.model.Item;
import com.fellows.shoppinglist.repository.ItemRepository;

import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

public class ItemHashSetRepositoryImpl implements ItemRepository {

    private final Collection<Item> items;

    public ItemHashSetRepositoryImpl() {
        items = new HashSet<>();
    }

    @Override
    public void add(Item entity) {
        if (!items.contains(entity)) {
            items.add(entity);
        }
    }

    @Override
    public boolean remove(Item entity) {
        return items.remove(entity);
    }

    @Override
    public boolean contains(Item entity) {
        return items.contains(entity);
    }

    @Override
    public Item find(String name) {
        return items.stream().filter(i -> i.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    @Override
    public Collection<Item> findAll() {
        return items;
    }

    @Override
    public Collection<Item> findAll(String partial) {
        return items.stream().filter(i -> i.getName().toLowerCase().contains(partial.toLowerCase())).collect(Collectors.toList());
    }
}
