package com.fellows.shoppinglist.repository;

import com.fellows.shoppinglist.model.Item;

public interface ItemRepository extends ICrud<String, Item> {

}
