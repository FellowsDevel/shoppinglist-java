package com.fellows.shoppinglist.repository;

import com.fellows.shoppinglist.model.ShoppingList;

import java.util.UUID;

public interface ShoppingListRepository extends ICrud<UUID, ShoppingList> {
}
