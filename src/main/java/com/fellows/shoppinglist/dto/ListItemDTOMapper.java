package com.fellows.shoppinglist.dto;

import com.fellows.shoppinglist.model.ListItem;

import java.util.function.Function;

public class ListItemDTOMapper implements Function<ListItem, ListItemDTO> {
    @Override
    public ListItemDTO apply(ListItem listItem) {
        ListItemDTO dto = new ListItemDTO();
        dto.setId(listItem.getId());
        dto.setItem(listItem.getItem());
        dto.setPurchasedAt(listItem.getPurchasedAt());
        dto.setPurchased(listItem.isPurchased());
        dto.setQuantity(listItem.getQuantity());
        return dto;
    }
}
