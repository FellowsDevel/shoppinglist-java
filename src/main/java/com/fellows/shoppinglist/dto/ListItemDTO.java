package com.fellows.shoppinglist.dto;

import com.fellows.shoppinglist.model.Item;
import com.fellows.shoppinglist.model.ListItem;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class ListItemDTO {

    private UUID id;
    private Item item;
    private LocalDateTime purchasedAt;
    private int quantity;
    private boolean purchased;

    public ListItemDTO() {

    }

    public static ListItemDTO of(ListItem listItem) {
        ListItemDTO dto = new ListItemDTO();
        dto.setId(listItem.getId());
        dto.setItem(listItem.getItem());
        dto.setPurchasedAt(listItem.getPurchasedAt());
        dto.setPurchased(listItem.isPurchased());
        return dto;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public LocalDateTime getPurchasedAt() {
        return purchasedAt;
    }

    public void setPurchasedAt(LocalDateTime purchasedAt) {
        this.purchasedAt = purchasedAt;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isPurchased() {
        return purchased;
    }

    public void setPurchased(boolean purchased) {
        this.purchased = purchased;
    }

    @Override
    public String toString() {
        return "ListItemDTO{" +
                "id=" + id +
                ", item=" + item +
                ", purchasedAt=" + purchasedAt +
                ", quantity=" + quantity +
                ", purchased=" + purchased +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListItemDTO that = (ListItemDTO) o;
        return quantity == that.quantity && purchased == that.purchased && Objects.equals(id, that.id) && Objects.equals(item, that.item) && Objects.equals(purchasedAt, that.purchasedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, item, purchasedAt, quantity, purchased);
    }
}
