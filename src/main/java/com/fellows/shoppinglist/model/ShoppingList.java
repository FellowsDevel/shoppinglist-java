package com.fellows.shoppinglist.model;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

public class ShoppingList implements Comparable{

    public static final int DEFAULT_REMAINING_DAYS = 8;
    private final LocalDate creationDate;
    private final Collection<ListItem> items;
    private UUID uuid;
    private String description;
    private int remainingDays;

    public ShoppingList(UUID uuid) {
        this(DEFAULT_REMAINING_DAYS);
        this.uuid = uuid;
    }

    public ShoppingList() {
        this(DEFAULT_REMAINING_DAYS);
    }

    public ShoppingList(int remainingDays) {
        this(LocalDate.now(), remainingDays);
    }

    public ShoppingList(LocalDate creationDate, int remainingDays) {
        this(creationDate, remainingDays, new HashSet<>());
    }

    public ShoppingList(LocalDate creationDate, int remainingDays, Collection<ListItem> items) {
        this.items = items;
        this.creationDate = creationDate;
        setRemainingDays(remainingDays);
        this.uuid = UUID.randomUUID();
    }

    public UUID getUid() {
        return uuid;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void addItem(ListItem item) {
        if (noItemFound(item)) {
            items.add(item);
        }
    }

    private boolean noItemFound(ListItem item) {
        return items.stream()
                .filter(i -> i.getItem().getName().equalsIgnoreCase(item.getItem().getName()))
                .findFirst().isEmpty();
    }

    public Collection<ListItem> getListItems() {
        return items;
    }

    public void removeItem(ListItem listItem) {
        items.remove(listItem);
    }

    public int getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(int days) {
        if (days > 0) {
            remainingDays = days;
        }
        if (remainingDays <= 0) {
            remainingDays = ShoppingList.DEFAULT_REMAINING_DAYS;
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void increaseRemainingDays() {
        remainingDays++;
    }

    public void decreaseRemainingDays() {
        if (remainingDays > 0) {
            remainingDays--;
        }
    }

    @Override
    public String toString() {

        return """
                ShoppingList {
                    uuid          = %s,
                    description   = %s,
                    remainingDays = %d,
                    creationDate  = %s
                    items         = %s
                }
                """.formatted(uuid, description, remainingDays, creationDate, items);
    }

    @Override
    public int compareTo(@NotNull Object o) {
        return uuid.toString().compareTo(((ShoppingList) o).uuid.toString());
    }
}
