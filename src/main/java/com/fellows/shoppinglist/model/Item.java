package com.fellows.shoppinglist.model;


public class Item implements Comparable<Item> {

    private String name;

    private Item(String name) {
        this.name = name;
    }

    public static Item of(String name){
        return new Item(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Item o) {
        return name.compareToIgnoreCase(o.getName());
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                '}';
    }
}
