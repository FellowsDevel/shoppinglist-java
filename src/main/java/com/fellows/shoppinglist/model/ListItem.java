package com.fellows.shoppinglist.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

public class ListItem implements Comparable<ListItem> {

    private final UUID id = UUID.randomUUID();
    private final Item item;
    private LocalDateTime purchasedAt;
    private int quantity;
    private boolean purchased;

    private ListItem(Item item) {
        this.item = item;
        this.quantity = 1;
        this.purchased = false;
    }

    public static ListItem of(Item item) {
        return new ListItem(item);
    }

    public Item getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int qtd) {
        if (qtd > 0) {
            this.quantity = qtd;
        }
    }

    public void increaseQuantity() {
        this.quantity++;
    }

    public void increaseQuantity(int qtd) {
        this.quantity += qtd;
    }

    public void decreaseQuantity() {
        if (this.quantity > 1) {
            this.quantity--;
        }
    }

    public void decreaseQuantity(int qtd) {
        if (this.quantity - qtd >= 1) {
            this.quantity -= qtd;
        }
    }

    public boolean isPurchased() {
        return purchased;
    }

    public void purchase() {
        purchased = true;
        purchasedAt = LocalDate.now().atStartOfDay();
    }

    public void returnItem() {
        this.purchased = false;
        purchasedAt = null;
    }

    public LocalDateTime getPurchasedAt() {
        return purchasedAt;
    }

    @Override
    public int compareTo(ListItem o) {
        return getItem().getName().compareToIgnoreCase(o.getItem().getName());
    }

    @Override
    public String toString() {
        return "ListItem{" +
                "id=" + id +
                ", item=" + item +
                ", purchasedAt=" + purchasedAt +
                ", quantity=" + quantity +
                ", purchased=" + purchased +
                '}';
    }

    public UUID getId() {
        return id;
    }

}
