package com.fellows.shoppinglist.service;

import com.fellows.shoppinglist.model.Item;
import com.fellows.shoppinglist.model.ListItem;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ItemDatesService {

    private final Map<Item, Collection<LocalDateTime>> items = new HashMap<>();

    private ItemDatesService() {
    }

    public static ItemDatesService create(){
        return new ItemDatesService();
    }

    private void addItem(Item item) {
        if (!items.containsKey(item)) {
            items.put(item, new HashSet<>());
        }
    }

    private int getAverageDays(Item item) {
        int avg;

        List<LocalDateTime> purchaseDates = new ArrayList<>(items.get(item));
        Collections.sort(purchaseDates);

        if (items.get(item).size() > 1) {
            List<Long> avgList = IntStream.range(0, purchaseDates.size() - 1)
                    .mapToObj(x -> getDuration(purchaseDates.get(x), purchaseDates.get(x + 1))).toList();
            avg = (int) Math.ceil(avgList.stream().mapToLong(Long::longValue).average().orElse(0));
        } else {
            avg = (int) getDuration(purchaseDates.get(0), LocalDateTime.now());
        }
        return avg;
    }

    private long getDuration(LocalDateTime older, LocalDateTime recent) {
        return Duration.between(older, recent).toDays();
    }

    public void add(LocalDateTime date, Item item) {
        addItem(item);
        items.get(item).add(date);
    }

    public int purchasedHowManyTimes(Item item) {
        return items.get(item).size();
    }

    public LocalDateTime lastPurchase(Item item) {
        return items.get(item).stream().max(LocalDateTime::compareTo).orElse(null);
    }

    public int getAverageDays(ListItem item) {
        return getAverageDays(item.getItem());
    }

    public Collection<Item> getItemsToBuy(int daysLeft) {
        return items.keySet()
                .stream()
                .filter(i -> getAverageDays(i) <= daysLeft)
                .collect(Collectors.toList());
    }
}
