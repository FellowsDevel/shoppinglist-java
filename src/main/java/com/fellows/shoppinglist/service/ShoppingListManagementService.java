package com.fellows.shoppinglist.service;

import com.fellows.shoppinglist.dao.ShoppingListDAO;
import com.fellows.shoppinglist.dao.impl.ShoppingListDAOImpl;
import com.fellows.shoppinglist.model.ShoppingList;

import java.util.Collection;
import java.util.UUID;

public class ShoppingListManagementService {

    private final ShoppingListDAO shoppingListDAO;

    private ShoppingListManagementService() {
        this(new ShoppingListDAOImpl());
    }

    private ShoppingListManagementService(ShoppingListDAO shoppingListDAO) {
        this.shoppingListDAO = shoppingListDAO;
    }

    public static ShoppingListManagementService create() {
        return new ShoppingListManagementService();
    }

    public void addList(ShoppingList shoppingList) {
        this.shoppingListDAO.add(shoppingList);
    }

    public boolean remove(ShoppingList shoppingList) {
        return this.shoppingListDAO.remove(shoppingList);
    }

    public ShoppingList getList(String uuid) {
        return shoppingListDAO.find(UUID.fromString(uuid));
    }

    public Collection<ShoppingList> findAll() {
        return shoppingListDAO.findAll().stream().sorted().toList();
    }
}
