package com.fellows.shoppinglist.service;

import com.fellows.shoppinglist.dao.ListItemDAO;
import com.fellows.shoppinglist.model.ListItem;

import java.time.LocalDateTime;
import java.util.*;

public class ListItemPurchaseHistoryService implements ListItemDAO {

    private final Map<LocalDateTime, Collection<ListItem>> items = new HashMap<>();

    private ListItemPurchaseHistoryService() {

    }

    public static ListItemPurchaseHistoryService create() {
        return new ListItemPurchaseHistoryService();
    }

    public Set<LocalDateTime> getDates() {
        return items.keySet();
    }

    public void addPurchaseDate(LocalDateTime purchaseDate) {
        if (!items.containsKey(purchaseDate)) {
            items.put(purchaseDate, new HashSet<>());
        }
    }

    public void addListItemCollection(LocalDateTime purchaseDate, Collection<ListItem> listItems) {
        addPurchaseDate((purchaseDate));
        items.get(purchaseDate).addAll(listItems);
    }

    public void add(LocalDateTime purchaseDate, ListItem item) {
        addPurchaseDate(purchaseDate);
        items.get(purchaseDate).add(item);
    }

    public Collection<ListItem> getItems(LocalDateTime purchaseDate) {
        return items.get(purchaseDate);
    }

    public boolean removeItem(LocalDateTime purchaseDate, ListItem item) {
        return items.getOrDefault(purchaseDate, new HashSet<>()).remove(item);
    }

    public boolean contains(LocalDateTime purchaseDate, ListItem item) {
        return items.get(purchaseDate).contains(item);
    }

}
