package com.fellows.shoppinglist.service;

import com.fellows.shoppinglist.dao.ItemDAO;
import com.fellows.shoppinglist.dao.impl.ItemDAOImpl;
import com.fellows.shoppinglist.model.Item;

import java.util.Collection;

public class ItemManagementService {

    private final ItemDAO itemDAO;

    private ItemManagementService() {
        this(new ItemDAOImpl());
    }

    private ItemManagementService(ItemDAO itemDAO) {
        this.itemDAO = itemDAO;
    }

    public static ItemManagementService create() {
        return new ItemManagementService();
    }

    public Collection<Item> getItems() {
        return itemDAO.findAll();
    }

    public void addItem(String itemName) {
        if (getItem(itemName) == null) {
            itemDAO.add(Item.of(itemName));
        }
    }

    public boolean remove(Item entity) {
        return itemDAO.remove(entity);
    }

    public Item getItem(String name) {
        return itemDAO.find(name);
    }
}
