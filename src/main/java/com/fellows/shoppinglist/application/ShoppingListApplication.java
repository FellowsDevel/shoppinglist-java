package com.fellows.shoppinglist.application;

import com.fellows.shoppinglist.model.Item;
import com.fellows.shoppinglist.model.ListItem;
import com.fellows.shoppinglist.model.ShoppingList;
import com.fellows.shoppinglist.service.ItemDatesService;
import com.fellows.shoppinglist.service.ItemManagementService;
import com.fellows.shoppinglist.service.ListItemPurchaseHistoryService;
import com.fellows.shoppinglist.service.ShoppingListManagementService;

import java.time.LocalDate;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ShoppingListApplication {

    private final static String SYSTEM_UUID = "000af017-1fea-4d03-a8b0-29f63c4c1975";
    private static ShoppingListApplication application;
    private final ItemManagementService itemManagementService;
    private final ShoppingListManagementService shoppingListManagementService;
    private final ListItemPurchaseHistoryService listItemPurchaseHistoryService;
    private final ItemDatesService itemDatesService;
    private final ShoppingList systemList;

    private ShoppingListApplication() {
        systemList = new ShoppingList(UUID.fromString(SYSTEM_UUID));
        systemList.setDescription("Lista do Sistema");
        itemManagementService = ItemManagementService.create();
        listItemPurchaseHistoryService = ListItemPurchaseHistoryService.create();
        itemDatesService = ItemDatesService.create();
        shoppingListManagementService = ShoppingListManagementService.create();
        shoppingListManagementService.addList(systemList);
    }

    public static ShoppingListApplication getInstance() {
        if (application == null) {
            application = new ShoppingListApplication();
        }
        return application;
    }


    private Collection<ListItem> listItemList(ShoppingList shoppingList) {
        return shoppingList.getListItems();
    }

    private ListItem addItem(ShoppingList list, String name) {
        return addItem(LocalDate.now(), list, name);
    }

    private ListItem addItem(LocalDate dateTime, ShoppingList list, String name) {
        itemManagementService.addItem(name);
        ListItem listItem = ListItem.of(itemManagementService.getItem(name));
        listItemPurchaseHistoryService.add(dateTime.atStartOfDay(), listItem);
        itemDatesService.add(dateTime.atStartOfDay(), listItem.getItem());
        list.addItem(listItem);
        return listItem;
    }

    private ListItem getListItem(ShoppingList list, String name) {
        return list.getListItems()
                .stream()
                .filter(i -> i.getItem().getName().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow();
    }

    private void increaseQuantity(ShoppingList list, String name, int qtd) {
        if (list == null) {
            return;
        }
        getListItem(list, name).increaseQuantity(qtd);
    }

    private void purchase(ShoppingList list, String itemName) {
        if (list == null) {
            return;
        }
        getListItem(list, itemName).purchase();
    }

    private void finalizePurchase(ShoppingList list) {
        if (list == null) {
            return;
        }
        getPurchasedItems(list)
                .collect(Collectors.toSet())
                .forEach(li -> {
                    itemDatesService.add(li.getPurchasedAt(), li.getItem());
                    listItemPurchaseHistoryService.add(li.getPurchasedAt(), li);
                });
        removePurchased(list);
    }

    private void removePurchased(ShoppingList list) {
        list.getListItems().removeAll(
                list.getListItems()
                        .stream()
                        .filter(ListItem::isPurchased)
                        .collect(Collectors.toSet()));
    }

    private Stream<ListItem> getPurchasedItems(ShoppingList list) {
        return list.getListItems()
                .stream()
                .filter(ListItem::isPurchased);
    }

    public final Collection<ShoppingList> listAllLists() {
        return shoppingListManagementService.findAll();
    }
    public final Collection<ListItem> listItemList() {
        return listItemList(systemList);
    }

    public final Collection<ListItem> listItemList(String uuid) {
        return listItemList(shoppingListManagementService.getList(uuid));
    }

    public final Collection<Item> listItems() {
        return itemManagementService.getItems();
    }

    public final ListItem addItem(String itemName) {
        return addItem(systemList, itemName);
    }

    public final ListItem addItem(LocalDate dateTime, String uuid, String itemName) {
        return addItem(dateTime, shoppingListManagementService.getList(uuid), itemName);
    }

    public final void increaseQuantity(String itemName) {
        increaseQuantity(systemList, itemName, 1);
    }

    public final void increaseQuantity(String itemName, int qtd) {
        increaseQuantity(systemList, itemName, qtd);
    }

    public final void purchase(String itemName) {
        purchase(systemList, itemName);
    }

    public final void purchase(String uuid, String itemName) {
        purchase(shoppingListManagementService.getList(uuid), itemName);
    }

    public final void finalizePurchase() {
        finalizePurchase(systemList);
    }

    public final void finalizePurchase(String uuid) {
        finalizePurchase(shoppingListManagementService.getList(uuid));
    }

    public final String createList(LocalDate creationDate, String description) {
        ShoppingList newList = new ShoppingList(creationDate, ShoppingList.DEFAULT_REMAINING_DAYS);
        newList.setDescription(description);
        shoppingListManagementService.addList(newList);
        return newList.getUid().toString();
    }

    public final int getAverageDays(String itemName) {
        ListItem listItem = ListItem.of(itemManagementService.getItem(itemName));
        return itemDatesService.getAverageDays(listItem);
    }

    public final Collection<Item> getItemsToBuy(int daysLeft) {
        return itemDatesService.getItemsToBuy(daysLeft);
    }
}
