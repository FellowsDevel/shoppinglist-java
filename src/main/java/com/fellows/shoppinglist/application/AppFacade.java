package com.fellows.shoppinglist.application;

import com.fellows.shoppinglist.dto.ListItemDTO;
import com.fellows.shoppinglist.dto.ListItemDTOMapper;
import com.fellows.shoppinglist.model.Item;
import com.fellows.shoppinglist.model.ListItem;
import com.fellows.shoppinglist.model.ShoppingList;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class AppFacade {
    private final ShoppingListApplication shoppingListApplication;
    private final ListItemDTOMapper listItemDTOMapper = new ListItemDTOMapper();

    private AppFacade() {
        shoppingListApplication = ShoppingListApplication.getInstance();
    }

    public static AppFacade create() {
        return new AppFacade();
    }

    private ListItemDTO convert(ListItem listItem) {
        return listItemDTOMapper.apply(listItem);
    }

    private Collection<ListItemDTO> convert(Collection<ListItem> col) {
        return col.stream().map(listItemDTOMapper).collect(Collectors.toList());
    }

    protected Collection<ListItemDTO> listItemList() {
        return convert(shoppingListApplication.listItemList());
    }
    protected Collection<ShoppingList> listAllLists() {
        return shoppingListApplication.listAllLists();
    }

    protected Collection<ListItemDTO> listItemList(String uuid) {
        return convert(shoppingListApplication.listItemList(uuid));
    }

    protected Collection<Item> listItems() {
        return shoppingListApplication.listItems();
    }

    protected ListItemDTO addItem(String itemName) {
        return convert(shoppingListApplication.addItem(itemName));
    }

    protected ListItemDTO addItem(LocalDate dateTime, String uuid, String itemName) {
        return convert(shoppingListApplication.addItem(dateTime, uuid, itemName));
    }

    protected Collection<ListItemDTO> addItens(LocalDate dateTime, String uuid, String... itemName) {
        return Arrays.stream(itemName).map(item ->
                convert(shoppingListApplication.addItem(dateTime, uuid, item))).toList();
    }

    protected void increaseQuantity(String itemName) {
        shoppingListApplication.increaseQuantity(itemName);
    }

    protected void increaseQuantity(String itemName, int qtd) {
        shoppingListApplication.increaseQuantity(itemName, qtd);
    }

    protected void purchase(String itemName) {
        shoppingListApplication.purchase(itemName);
    }

    protected void purchase(String uuid, String itemName) {
        shoppingListApplication.purchase(uuid, itemName);
    }

    protected void finalizePurchase() {
        shoppingListApplication.finalizePurchase();
    }

    protected void finalizePurchase(String uuid) {
        shoppingListApplication.finalizePurchase(uuid);
    }

    protected String createList(LocalDate creationDate, String description) {
        return shoppingListApplication.createList(creationDate, description);
    }

    protected int getAverageDays(String itemName) {
        return shoppingListApplication.getAverageDays(itemName);
    }

    protected Collection<Item> getItemsToBuy(int daysLeft) {
        return shoppingListApplication.getItemsToBuy(daysLeft);
    }

    public void printListItems() {
        listItemList().forEach(System.out::println);
    }

    public void printItems() {
        listItems().forEach(System.out::println);
    }

    public void printListItemsFromList(String uuidItemList) {
        listItemList(uuidItemList).forEach(System.out::println);
    }
}
