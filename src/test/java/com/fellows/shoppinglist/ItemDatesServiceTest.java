package com.fellows.shoppinglist;

import com.fellows.shoppinglist.model.Item;
import com.fellows.shoppinglist.model.ListItem;
import com.fellows.shoppinglist.model.ShoppingList;
import com.fellows.shoppinglist.service.ItemDatesService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

class ItemDatesServiceTest {

    @Test
    void addPurchaseDate() {
        var item = Item.of("Rice");
        var itemList = ListItem.of(item);

        LocalDateTime now = LocalDate.now().atStartOfDay();
        LocalDateTime timeAgo = now.minusDays(ShoppingList.DEFAULT_REMAINING_DAYS);
        var dateList = ItemDatesService.create();

        dateList.add(now, itemList.getItem());
        dateList.add(timeAgo, itemList.getItem());

        Assertions.assertEquals(2, dateList.purchasedHowManyTimes(itemList.getItem()));
    }

    @Test
    void getAverageDays() {
        var item = Item.of("Rice");
        var itemList = ListItem.of(item);

        LocalDateTime eightDays = LocalDate.now().atStartOfDay().minusDays(8);
        LocalDateTime fourDays = LocalDate.now().atStartOfDay().minusDays(4);

        var dateList = ItemDatesService.create();
        dateList.add(fourDays, itemList.getItem());
        dateList.add(eightDays, itemList.getItem());

        Assertions.assertEquals(4, dateList.getAverageDays(itemList));
    }

    @Test
    void getAverageDaysForFourDaysOfTwo() {
        var item = Item.of("Rice");
        var itemList = ListItem.of(item);

        LocalDateTime twoDays = LocalDate.now().atStartOfDay().minusDays(2);
        LocalDateTime fourDays = LocalDate.now().atStartOfDay().minusDays(4);
        LocalDateTime sixDays = LocalDate.now().atStartOfDay().minusDays(6);
        LocalDateTime eightDays = LocalDate.now().atStartOfDay().minusDays(8);

        var dateList = ItemDatesService.create();
        dateList.add(eightDays, itemList.getItem());
        dateList.add(twoDays, itemList.getItem());
        dateList.add(sixDays, itemList.getItem());
        dateList.add(fourDays, itemList.getItem());

        Assertions.assertEquals(2, dateList.getAverageDays(itemList));
    }

    @Test
    void getAverageDaysForOnePurchase() {
        var item = Item.of("Rice");
        var itemList = ListItem.of(item);

        LocalDateTime eightDaysAgo = LocalDate.now().atStartOfDay().minusDays(8);

        var dateList = ItemDatesService.create();
        dateList.add(eightDaysAgo, itemList.getItem());

        Assertions.assertEquals(ShoppingList.DEFAULT_REMAINING_DAYS, dateList.getAverageDays(itemList));
    }

    @Test
    void getLastPurchase() {
        var item = Item.of("Rice");
        var itemList = ListItem.of(item);

        LocalDateTime sixteenDaysAgo = LocalDate.now().atStartOfDay().minusDays(8);
        LocalDateTime eightDaysAgo = LocalDate.now().atStartOfDay().minusDays(4);

        var dateList = ItemDatesService.create();
        dateList.add(sixteenDaysAgo, itemList.getItem());
        dateList.add(eightDaysAgo, itemList.getItem());

        Assertions.assertEquals(eightDaysAgo, dateList.lastPurchase(itemList.getItem()));
    }

}