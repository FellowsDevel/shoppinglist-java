package com.fellows.shoppinglist;

import com.fellows.shoppinglist.service.ListItemPurchaseHistoryService;
import com.fellows.shoppinglist.model.Item;
import com.fellows.shoppinglist.model.ListItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

class ListItemPurchaseHistoryServiceTest {

    @Test
    void addItem() {
        var ldi = ListItemPurchaseHistoryService.create();
        var now = LocalDate.now().atStartOfDay();

        var rice = ListItem.of(Item.of("Rice"));
        var meat = ListItem.of(Item.of("Meat"));

        ldi.add(now, rice);
        Assertions.assertEquals(1, ldi.getItems(now).size());
        ldi.add(now, meat);
        Assertions.assertEquals(2, ldi.getItems(now).size());
    }

    @Test
    void getItems() {
        var ldi = ListItemPurchaseHistoryService.create();
        var now = LocalDate.now().atStartOfDay();
        ldi.addPurchaseDate(now);

        var rice = ListItem.of(Item.of("Rice"));
        var meat = ListItem.of(Item.of("Meat"));


        ldi.add(now, rice);
        Assertions.assertEquals(1, ldi.getItems(now).size());
        ldi.add(now, meat);
        Assertions.assertEquals(2, ldi.getItems(now).size());
    }

    @Test
    void getDates() {
        var ldi = ListItemPurchaseHistoryService.create();
        var now = LocalDate.now().atStartOfDay();
        ldi.addPurchaseDate(now);

        var rice = ListItem.of(Item.of("Rice"));

        ldi.add(now, rice);
        Assertions.assertTrue(ldi.getDates().contains(now));
        Assertions.assertTrue(ldi.getItems(now).contains(rice));
    }

    @Test
    void removeItem() {
        var ldi = ListItemPurchaseHistoryService.create();
        var now = LocalDate.now().atStartOfDay();
        ldi.addPurchaseDate(now);

        var rice = ListItem.of(Item.of("Rice"));
        var meat = ListItem.of(Item.of("Meat"));


        ldi.add(now, rice);
        Assertions.assertEquals(1, ldi.getItems(now).size());
        ldi.add(now, meat);
        Assertions.assertEquals(2, ldi.getItems(now).size());
        Assertions.assertTrue(ldi.removeItem(now, rice));
        Assertions.assertEquals(1, ldi.getItems(now).size());
    }

    @Test
    void contains() {
        var ldi = ListItemPurchaseHistoryService.create();
        var now = LocalDate.now().atStartOfDay();
        ldi.addPurchaseDate(now);

        var rice = ListItem.of(Item.of("Rice"));
        var meat = ListItem.of(Item.of("Meat"));


        ldi.add(now, rice);
        ldi.add(now, meat);
        Assertions.assertTrue(ldi.getItems(now).contains(rice));
        Assertions.assertEquals(2, ldi.getItems(now).size());
        Assertions.assertTrue(ldi.removeItem(now, rice));
        Assertions.assertEquals(1, ldi.getItems(now).size());
        Assertions.assertFalse(ldi.contains(now, rice));
    }
}