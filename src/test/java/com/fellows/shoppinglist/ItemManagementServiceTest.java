package com.fellows.shoppinglist;

import com.fellows.shoppinglist.service.ItemManagementService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ItemManagementServiceTest {

    @Test
    void itemListIsEmpty(){
        ItemManagementService im = ItemManagementService.create();
        Assertions.assertTrue(im.getItems().isEmpty());
    }

    @Test
    void itemList(){

        ItemManagementService im = ItemManagementService.create();
        Assertions.assertTrue(im.getItems().isEmpty());
    }

}