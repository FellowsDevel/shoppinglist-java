package com.fellows.shoppinglist;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import com.fellows.shoppinglist.model.Item;
import com.fellows.shoppinglist.model.ListItem;
import com.fellows.shoppinglist.model.ShoppingList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ShoppingListDAOTest {

    @Test
    @DisplayName("Shopping list has a creation date")
    void creationDateTest() {
        ShoppingList sl = new ShoppingList();

        LocalDate slCreationDate = sl.getCreationDate();
        LocalDate now = LocalDate.now();

        assertEquals(now.getDayOfMonth(), slCreationDate.getDayOfMonth());
        assertEquals(now.getMonth(), slCreationDate.getMonth());
        assertEquals(now.getYear(), slCreationDate.getYear());
    }

    @Test
    @DisplayName("It is possible to add an Item to the list")
    void addItemToListTest() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        assertEquals(1, sl.getListItems().size());
    }

    @Test
    @DisplayName("It is possible to remove an Item from the list")
    void removeItemFromListTest() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        assertEquals(1, sl.getListItems().size());
        sl.removeItem(sl.getListItems().stream().findFirst().orElse(null));
        assertEquals(0, sl.getListItems().size());
    }

    @Test
    @DisplayName("The default item quantity is 1")
    void defaultItemQuantityTest() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        ListItem li = sl.getListItems().stream().findFirst().orElseThrow();
        assertEquals(1, li.getQuantity());
    }

    @Test
    @DisplayName("Cannot decrease item quantity by one below 1")
    void decreaseItemQuantityByOneBelowOneTest() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        ListItem li = sl.getListItems().stream().findFirst().orElseThrow();
        assertEquals(1, li.getQuantity());
        li.decreaseQuantity();
        li.decreaseQuantity();
        assertEquals(1, li.getQuantity());
    }

    @Test
    @DisplayName("It is not possible to set zero or negative quantity")
    void setZeroAsDefaultQuantity() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        ListItem li = sl.getListItems().stream().findFirst().orElseThrow();
        li.setQuantity(0);
        assertEquals(1, li.getQuantity());
        li.setQuantity(-2);
        assertEquals(1, li.getQuantity());
    }

    @Test
    @DisplayName("Cannot decrease item quantity by specific amount below 1")
    void decreaseItemQuantityBelowOneTest() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        ListItem li = sl.getListItems().stream().findFirst().orElseThrow();
        assertEquals(1, li.getQuantity());
        li.decreaseQuantity(3);
        assertEquals(1, li.getQuantity());
    }

    @Test
    @DisplayName("It is possible to increase one by one an item quantity on list")
    void increaseItemQuantityTest() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        ListItem li = sl.getListItems().stream().findFirst().orElseThrow();
        li.increaseQuantity();
        assertEquals(2, li.getQuantity());
        li.increaseQuantity();
        assertEquals(3, li.getQuantity());
    }

    @Test
    @DisplayName("It is possible to increase specific amount to an item on list")
    void increaseItemQuantityAmountTest() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        ListItem li = sl.getListItems().stream().findFirst().orElseThrow();
        li.increaseQuantity(5);
        assertEquals(6, li.getQuantity());
    }

    @Test
    @DisplayName("It is possible to decrease one by one an item quantity on list")
    void decreaseItemQuantityTest() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        ListItem li = sl.getListItems().stream().findFirst().orElseThrow();
        li.setQuantity(2);
        assertEquals(2, li.getQuantity());
        li.decreaseQuantity();
        assertEquals(1, li.getQuantity());
    }

    @Test
    @DisplayName("It is possible to decrease specific amount to an item on list")
    void decreaseItemQuantityAmountTest() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        ListItem li = sl.getListItems().stream().findFirst().orElseThrow();
        li.setQuantity(10);
        li.decreaseQuantity(5);
        assertEquals(5, li.getQuantity());
    }

    @Test
    @DisplayName("It is possible to change the item name on the list")
    void changeItemNameOnList() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        Item item = sl.getListItems().stream().findFirst().orElseThrow().getItem();
        item.setName("Pao ciabatta");
        assertEquals("Pao ciabatta", sl.getListItems().stream().findFirst().orElseThrow().getItem().getName());
    }

    @Test
    @DisplayName("It is possible to purchase an item on the list")
    void purchaseAnItem() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        ListItem item = sl.getListItems().stream().findFirst().orElseThrow();
        item.purchase();
        assertTrue(item.isPurchased());
    }

    @Test
    @DisplayName("It is possible to return a purchased item on the list")
    void returnAnItemFromList() {
        ShoppingList sl = new ShoppingList();
        sl.addItem(ListItem.of(Item.of("Pão francês")));
        ListItem item = sl.getListItems().stream().findFirst().orElseThrow();
        item.purchase();
        assertTrue(item.isPurchased());
        item.returnItem();
        assertFalse(item.isPurchased());
    }

    @Test
    @DisplayName("The default items remaining days on list is 8")
    void listHasDefaultRemainingDays() {
        var sl = new ShoppingList();
        assertEquals(8, sl.getRemainingDays());
    }

    @Test
    @DisplayName("It is possible to set remaining days to list")
    void setRemainingDays() {
        int days = 93;
        var sl = new ShoppingList();
        sl.setRemainingDays(days);
        assertEquals(days, sl.getRemainingDays());
    }

    @Test
    @DisplayName("It is possible to set a description to a list")
    void setDescriptionToList() {
        var listName = "Weekly list";
        var sl = new ShoppingList();
        sl.setDescription(listName);
        assertEquals(listName, sl.getDescription());
    }

    @Test
    @DisplayName("It is possible to increase a remaining days of a list")
    void increaseRemainingDays() {
        var sl = new ShoppingList();
        sl.increaseRemainingDays();
        assertEquals(9, sl.getRemainingDays());
    }

    @Test
    @DisplayName("It is possible to decrease a remaining days of a list")
    void decreaseRemainingDays() {
        var sl = new ShoppingList();
        sl.decreaseRemainingDays();
        assertEquals(7, sl.getRemainingDays());
    }
}
