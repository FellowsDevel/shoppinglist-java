package com.fellows.shoppinglist.application;

import com.fellows.shoppinglist.model.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ShoppingListApplicationTest {

    AppFacade shopList;

    @BeforeEach
    void setUp() {
        shopList = AppFacade.create();
    }

    @Test
    void app() {

        var rice = "Rice";
        var meat = "Meat";
        var corn = "Corn";

        shopList.addItem(rice);
        shopList.addItem(meat);
        shopList.addItem(meat);
        shopList.addItem(corn);

        shopList.increaseQuantity(rice);
        shopList.increaseQuantity(meat);
        shopList.increaseQuantity(corn, 2);

        System.out.println("************************ - 1");
        shopList.printListItems();
        shopList.printItems();
        System.out.println("************************ - 1");

        shopList.purchase(rice);
        shopList.purchase(corn);

        System.out.println("************************ - 2");
        shopList.printListItems();
        shopList.printItems();
        System.out.println("************************ - 2");

        shopList.finalizePurchase();

        System.out.println("************************ - 3");
        shopList.printListItems();
        shopList.printItems();
        System.out.println("************************ - 3");

        LocalDate eightDaysAgo = LocalDate.now().minusDays(8);
        LocalDate sixDaysAgo = LocalDate.now().minusDays(6);

        String uuidSemanal01 = shopList.createList(eightDaysAgo, "Semanal 01");
        String uuidSemanal02 = shopList.createList(sixDaysAgo, "Semanal 02");

        shopList.addItem(sixDaysAgo, uuidSemanal01, rice);
//        shopList.addItem(sixDaysAgo, uuidSemanal01, meat);
//        shopList.addItem(sixDaysAgo, uuidSemanal01, corn);
        shopList.addItens(sixDaysAgo, uuidSemanal01, meat, corn);

//        shopList.addItem(eightDaysAgo, uuidSemanal02, rice);
//        shopList.addItem(eightDaysAgo, uuidSemanal02, meat);
        shopList.addItens(eightDaysAgo, uuidSemanal02, rice, meat);

        System.out.println("************************ - 4");
        shopList.printListItemsFromList(uuidSemanal01);
        shopList.printItems();
        System.out.println("************************ - 4");

        shopList.purchase(uuidSemanal01, rice);
        shopList.purchase(uuidSemanal01, corn);
        shopList.purchase(uuidSemanal01, meat);

        shopList.purchase(uuidSemanal02, rice);
        shopList.purchase(uuidSemanal02, meat);

        shopList.finalizePurchase(uuidSemanal01);
        shopList.finalizePurchase(uuidSemanal02);

        System.out.println("************************ - 5");
        shopList.printListItemsFromList(uuidSemanal02);
        shopList.printItems();
        System.out.println("************************ - 5");

        System.out.println(shopList.getAverageDays(corn));
        System.out.println(shopList.getAverageDays(rice));
        System.out.println(shopList.getAverageDays(meat));

        assertEquals(4, shopList.getAverageDays(rice), "Average days is wrong");
        assertEquals(4, shopList.getAverageDays(meat), "Average days is wrong");
        assertEquals(6, shopList.getAverageDays(corn), "Average days is wrong");

        System.out.println("************************ - 6");
        assertEquals(2, shopList.getItemsToBuy(4).size(), "Items qtd is wrong");
        assertTrue(
                shopList.getItemsToBuy(4).stream().map(Item::getName)
                        .toList()
                        .containsAll(Arrays.asList("Meat", "Rice")),
                "Items names does not match");
        System.out.println("************************ - 6");

        System.out.println("************************ - 7");
        assertEquals(3, shopList.getItemsToBuy(6).size(), "Items qtd is wrong");
        assertTrue(
                shopList.getItemsToBuy(6).stream().map(Item::getName)
                        .toList()
                        .containsAll(Arrays.asList("Meat", "Rice", "Corn")),
                "Items names does not match");
        System.out.println("************************ - 7");


        shopList.listAllLists().forEach(System.out::println);

    }
}